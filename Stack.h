/*
Name: John McDonnell.
Student No: D00096987.
Course: Computing Level 8.
Group: 2.
Year: 2.
Module: Data Structures for Games.
CA2 - Reverse Polish Notation.Lecturer: Dr Derek Flood.Due Date: 10.04.2015.
Version: Final.
*/

#ifndef STACK_H_
#define STACK_H_

template<class T>

class Stack 
{

private:
	// up to you, but you MUST have an instance of a linked list or an array here.
	T* m_array;

	// declaring variables.
	int m_top;
	int m_size;
	int a_size;
	int grow_size;

public:
	// constructors.
	Stack()
	{
		a_size = 10;
		m_array = new T[a_size];
		m_size = 10;
		m_top = 0;
		grow_size = 10;
	}

	Stack(int a_new_size)
	{
		m_array = new T[a_new_size];
		m_size = 10;
		m_top = 0;
		a_size = a_new_size;
		grow_size = 10;
	}

	// pushes item on to the stack.
	void push(T item)
	{
		// if array is full.
		 if (m_top == m_size)
		{
			// create a temp array.
			T *temp_array = m_array;
			// new array = current size + the grow size.
			m_array = new T[m_size + grow_size];

			int x = 0;

			while(x < m_size)
			{
				// pushing each element into the new larger array.
				m_array[x] = temp_array[x]; 
				x++;
			}

			m_array[m_size] = item;
			m_size++;
			m_top++;

			// delete the temp array from memory
			delete[] temp_array; 
		}
		else
		{
			m_array[m_top] = item;
			m_top++;
		}
	}

	// returns the topmost item on the stack.
	T top()
	{
		int limit = 1;
		if (m_top > 0)
		{
			return m_array[m_top - limit];
		}
		return T();
	}
	// returns the topmost item on the stack and removes it.
	T pop()
	{
		int limit = 0;
		T returned_value = top();
		if (m_top > limit)
		{
			m_top--;
		}
		return returned_value;
	}



	// return true if there are no items on the stack.
	bool isEmpty()
	{
		int limit = 0;
		if (m_top > limit)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	// remove all items from the stack.
	void clear()
	{
		// delete array, create a new one. Reset sizes and top.
		delete[] m_array; 
		m_array = new T[a_size];
		m_size = a_size;
		m_top = 0;
	}
	// return the top value on the stack.
	int getm_top()
	{
		return m_top;
	}
};
#endif 