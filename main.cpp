/*
Name: John McDonnell.
Student No: D00096987.
Course: Computing Level 8.
Group: 2.
Year: 2.
Module: Data Structures for Games.
CA2 - Reverse Polish Notation.Lecturer: Dr Derek Flood.Due Date: 10.04.2015.
Version: Final.
*/

// header files.
#include "RPNCalculator.h"
#include "Stack.h"

// libraries 
#include <iostream>
#include <sstream>
using namespace std;


int main()
{
	// creating RPN Calculator
	RPNCalculator<double>* calculator = new RPNCalculator<double>();

	// interface and options for the user.
	cout << "RPN Calculator" << endl;
	cout << "(c) 2015 John McDonnell" << endl;
	cout << "Enter c to clear the stack" << endl;
	cout << "s to square" << endl;
	cout << "n to negate" << endl;
	cout << "p to pop current value" << endl;
	cout << "q to quit" << endl;
	cout << "X" << " >";
	
	// declaring varibales
	string input;
	char *c;
	double userInputOne;


	// taking users input.
	cin >> input;
	
	// looping through until the user wants to quit the application.
	while (input != "q")
	{
		// Parses the C-string str interpreting its content as a floating point number and returns its value as a double.
		userInputOne = strtod(input.c_str(), &c);

		// user options for the application.
		if (input == "c")
		{
			calculator->clear();
		}
		else if (input == "s")
		{
			calculator->square();
		}
		else if (input == "n")
		{
			calculator->negate();
		}
		else if (input == "p")
		{
			calculator->pop();
		}
		else if (input == "+")
		{
			calculator->add();
		}
		else if (input == "-")
		{
			calculator->subtract();
		}
		else if (input == "/")
		{
			calculator->divide();
		}
		else if (input == "*")
		{
			calculator->multiply();
		}
		else
		{
			calculator->push(userInputOne);
		}

		// display if calculator is empty.
		if (calculator->isEmpty())
		{
			cout << "X" << " >";
		}
		else
		{
			// get the current value on the stack.
			cout << calculator->value() << " >";

		}
		//take the user again.
		cin >> input;

	}
			
	return 0;
};