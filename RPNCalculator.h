/* 
Name: John McDonnell.
Student No: D00096987.
Course: Computing Level 8.
Group: 2.
Year: 2.
Module: Data Structures for Games.
CA2 - Reverse Polish Notation.
Lecturer: Dr Derek Flood.
Due Date: 10.04.2015.
Version: Final.
*/

#ifndef RPNCALCULATOR_H_
#define RPNCALCULATOR_H_
#include "Stack.h"
#include <cmath>

template<class T>

class RPNCalculator 
{
private:
	// you MUST use an instance of Stack from Stack.h

	//Stack T* m_array ;

	Stack<T>* m_array;

public:
	// declaring variables
	int a_size;

	// constructor.
	RPNCalculator()
	{
		a_size = 10;
		m_array = new Stack<T>(a_size);
	}
	// destructor.
	~RPNCalculator()
	{

	}

	// pushes a new operand onto the stack
	// the following operations are to be performed as defined for Reverse Polish Notation
	// binary operators:
	void push(T operand)
	{
		m_array->push(operand);
	}
	// add two values together
	void add()
	{
		int limit = 2;
		if (m_array->getm_top() < limit)
		{
			m_array->clear();
		}
		else
		{
			push(m_array->pop() + m_array->pop());
		}
	}
	// subtract two values
	void subtract()
	{
		int limit = 2;
		if (m_array->getm_top() < limit)
		{
			m_array->clear();
		}
		else
		{
			T first_value = m_array->pop();
			T second_value = m_array->pop();

			//check if right order in relation to polish 
			push(second_value - first_value);
		}
	}
	// multiply two values
	void multiply()
	{
		int limit = 2;
		if (m_array->getm_top() < limit)
		{
			m_array->clear();
		}
		else
		{
			push(m_array->pop() * m_array->pop());
		}
	}
	// divide two values
	void divide()
	{
		int limit = 2;
		if (m_array->getm_top() < limit)
		{
			m_array->clear();
		}
		else
		{
			T first_value = m_array->pop();
			T second_value = m_array->pop();

			//check if right order in relation to polish 
			push(second_value / first_value);
		}

	}

	// unary operators:

	// squares the current value
	void square()
	{
		float power_value = 2.0;

		T element_value = m_array->pop();
		push((T)pow(element_value, power_value));

	}

	// negates, i.e. 3 becomes -3
	void negate()
	{
		T element_value = m_array->pop();
		push(-element_value);
	}

	// tests to see if there are elements on the stack
	bool isEmpty()
	{
		return m_array->isEmpty();
	}

	// clears out the stack
	void clear()
	{
		m_array->clear();
	}

	// returns the topmost value
	T value()
	{
		return m_array->top();
	}

	// returns the topmost value and pops it off the top
	T pop()
	{
		return m_array->pop();
	}

};
#endif 